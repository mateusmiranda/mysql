#/bin/bash

if [ ! -f /var/lib/mysql/ibdata1 ]; then
    
    echo "Database not found in this container! Let's recreate it"
    mysql_install_db

    # Set Percona functions asked on installation
    /usr/bin/mysqld_safe & sleep 10s

    mysql -e "GRANT ALL ON *.* TO admin@'%' IDENTIFIED BY 'admin' WITH GRANT OPTION; FLUSH PRIVILEGES"
    mysql -e "CREATE FUNCTION fnv1a_64 RETURNS INTEGER SONAME 'libfnv1a_udf.so'"
    mysql -e "CREATE FUNCTION fnv_64 RETURNS INTEGER SONAME 'libfnv_udf.so'"
    mysql -e "CREATE FUNCTION murmur_hash RETURNS INTEGER SONAME 'libmurmur_udf.so'"

    # Restore Database if the file exists
    if [ -f /tmp/my_dump.sql.bz2 ]; then
        bunzip2 < /tmp/my_dump.sql.bz2 | mysql
    
        bunzip2 < /tmp/my_dump.sql.bz2 | mysql
        rm /tmp/my_dump.sql.bz2
    fi
    
    killall mysqld
    sleep 10s

fi
echo "Starting Mysql Server..."
/usr/bin/mysqld_safe
